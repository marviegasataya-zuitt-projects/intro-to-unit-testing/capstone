const { exchangeRates } = require('../src/util.js');

module.exports = (app) => {
	app.get('/', (req, res) => {
		return res.send({'data': {} });
	});

	app.get('/rates', (req, res) => {
		return res.send({
			rates: exchangeRates
		});
	})

	app.post('/currency2', (req, res) => {
		// create if conditions here to check the currency

		// check if all feilds are complete
		if(req.body.hasOwnProperty('alias') && req.body.hasOwnProperty('name') && req.body.hasOwnProperty('ex')){
			// check if duplicate alias is found in the data of util.js
			if(exchangeRates[req.body.alias] != null){
				return res.status(200).send({"Error": "Duplicate Alias Found!"})
			}
		}

		//return status 200 if route is running
		return res.status(200).send({
			'Message': 'Route is running'
		})
	})
}
