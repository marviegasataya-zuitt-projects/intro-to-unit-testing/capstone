const { exchangeRates } = require('../src/util.js');

module.exports = (app) => {
	app.get('/', (req, res) => {
		return res.send({'data': {} });
	});

	app.get('/rates', (req, res) => {
		return res.send({
			rates: exchangeRates
		});
	})

	app.post('/currency', (req, res) => {
		// create if conditions here to check the currency
		if(!req.body.hasOwnProperty("name")){
			return res.status(400).send({"Error": "Bad request - missing name parameter"})
		}

		if(typeof(req.body.name) != "string"){
			return res.status(400).send({"Error": "Bad request - name is not a string"})
		}

		if(req.body.name == ""){
			return res.status(400).send({"Error": "Bad request - name is empty"})
		}

		if(!req.body.hasOwnProperty('ex')){
			return res.status(400).send({"Error": "Bad request -  missing ex parameter"})
		}

		if(typeof(req.body.ex) != "object"){
			return res.status(400).send({"Error": "Bad request - ex is not an object"})
		}

		if(Object.keys(req.body.ex).length === 0 && req.body.ex.constructor === Object){
			return res.status(400).send({"Error": "Bad request - ex is empty"})
		}

		if(!req.body.hasOwnProperty("alias")){
			return res.status(400).send({"Error": "Bad request - missing alias parameter"})
		}

		if(typeof(req.body.alias) != "string"){
			return res.status(400).send({"Error": "Bad request - alias is not a string"})
		}

		if(req.body.alias == ""){
			return res.status(400).send({"Error": "Bad request - alias is empty"})
		}		

		// check if all feilds are complete
		if(req.body.hasOwnProperty('alias') && req.body.hasOwnProperty('name') && req.body.hasOwnProperty('ex')){
			// check if duplicate alias is found in the data of util.js
			if(exchangeRates[req.body.alias] != null){
				return res.status(400).send({"Error": "Duplicate Alias Found!"})
			}
		}

		//return status 200 if route is running
		return res.status(200).send({
			'Message': 'Route is running'
		})
	})
}
