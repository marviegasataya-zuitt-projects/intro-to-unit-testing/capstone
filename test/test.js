const chai = require('chai');
const { expect} = require('chai');
const http = require('chai-http');
chai.use(http);

describe('forex_api_test_suite_part_2', (done) => {
	//add the solutions here
	const domain = 'http://localhost:5001';

	//12. Check if post /currency returns status 200 if all fields are complete and there are no duplicates
	it('POST /currency returns status 200 if all fields are complete and there are no duplicates', (done) => {
		chai.request(domain)
		.post('/currency2')
		.type('json')
		.send({
            alias: 'riyadh',
            name: 'Saudi Arabian Riyadh',
            ex: {
                'peso': 0.47,
                'usd': 0.0092,
                'won': 10.93,
                'yuan': 0.065
            }

		})
		.end((err, res) =>{
			expect(res.status).to.equal(200); //Good Request
			done()
		})
	})
})
