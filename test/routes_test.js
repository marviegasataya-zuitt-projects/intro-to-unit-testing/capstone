const chai = require('chai');
const { expect} = require('chai');
const http = require('chai-http');
chai.use(http);

describe('forex_api_test_suite', (done) => {
	//add the solutions here
	const domain = 'http://localhost:5001';

	// Check if post /currency is running
	it('POST /currency endpoint is running', (done) =>{
		chai.request(domain)
		.post('/currency')
		.end((err, res) =>{
			expect(res).to.not.equal(undefined);
			done()
		})
	})

	// Check if post /currency returns status 400 if name is missing
	it('POST /currency endpoint name is missing', (done) => {
		chai.request(domain)
		.post('/currency')
		.type('json')
		.send({
			alias: 'riyadh',
			ex: {
				'peso': 0.47,
				'usd': 0.0092,
				'won': 10.93,
				'yuan': 0.065
			}
		})
		.end((err, res) =>{
			expect(res.status).to.equal(400); //Bad request
			done()
		})
	})

	// Check if post /currency returns status 400 if name is not a string
	it('POST /currency endpoint name is not a string', (done) => {
		chai.request(domain)
		.post('/currency')
		.type('json')
		.send({
			alias: 'riyadh',
			name: 09123,
			ex: {
				'peso': 0.47,
				'usd': 0.0092,
				'won': 10.93,
				'yuan': 0.065
			}
		})
		.end((err, res) =>{
			expect(res.status).to.equal(400); //Bad request
			done()
		})
	})

	// Check if post /currency returns status 400 if name is empty
	it('POST /currency endpoint name is not empty', (done) => {
		chai.request(domain)
		.post('/currency')
		.type('json')
		.send({
			alias: 'riyadh',
			name: "",
			ex: {
				'peso': 0.47,
				'usd': 0.0092,
				'won': 10.93,
				'yuan': 0.065
			}
		})
		.end((err, res) =>{
			expect(res.status).to.equal(400); //Bad request
			done()
		})
	})

	// Check if post /currency returns status 400 if ex is missing
	it('POST /currency endpoint ex is missing', (done) => {
		chai.request(domain)
		.post('/currency')
		.type('json')
		.send({
			alias: 'riyadh',
			name: 'Saudi Arabian Riyadh',
		})
		.end((err, res) =>{
			expect(res.status).to.equal(400); //Bad request
			done()
		})
	})

	// Check if post /currency returns status 400 if ex is not an object
	it('POST /currency endpoint ex is not an object', (done) => {
		chai.request(domain)
		.post('/currency')
		.type('json')
		.send({
			alias: 'riyadh',
			name: 'Saudi Arabian Riyadh',
			ex: 1212
		})
		.end((err, res) =>{
			expect(res.status).to.equal(400); //Bad request
			done()
		})
	})

	// Check if post /currency returns status 400 if ex is empty
	it('POST /currency endpoint ex is empty', (done) => {
		chai.request(domain)
		.post('/currency')
		.type('json')
		.send({
			alias: 'riyadh',
			name: 'Saudi Arabian Riyadh',
			ex: {

			}
		})
		.end((err, res) =>{
			expect(res.status).to.equal(400); //Bad request
			done()
		})
	})

	// Check if post /currency returns status 400 if alias is missing
	it('POST /currency alias is missing', (done) => {
		chai.request(domain)
		.post('/currency')
		.type('json')
		.send({
			name: 'Saudi Arabian Riyadh',
			ex: {
				'peso': 0.47,
				'usd': 0.0092,
				'won': 10.93,
				'yuan': 0.065
			}
		})
		.end((err, res) =>{
			expect(res.status).to.equal(400); //Bad request
			done()
		})
	})

	
	// Check if post /currency returns status 400 if alias is not an string
	it('POST /currency alias is not a string', (done) => {
		chai.request(domain)
		.post('/currency')
		.type('json')
		.send({
			alias: 23123,
			name: 'Saudi Arabian Riyadh',
			ex: {
				'peso': 0.47,
				'usd': 0.0092,
				'won': 10.93,
				'yuan': 0.065
			}
		})
		.end((err, res) =>{
			expect(res.status).to.equal(400); //Bad request
			done()
		})
	})


	// Check if post /currency returns status 400 if alias is empty
	it('POST /currency alias is empty', (done) => {
		chai.request(domain)
		.post('/currency')
		.type('json')
		.send({
			alias: "",
			name: 'Saudi Arabian Riyadh',
			ex: {
				'peso': 0.47,
				'usd': 0.0092,
				'won': 10.93,
				'yuan': 0.065
			}
		})
		.end((err, res) =>{
			expect(res.status).to.equal(400); //Bad request
			done()
		})
	})

	// Check if post /currency returns status 400 if all fields are complete but there is a duplicate alias
	it('POST /currency alias has a duplicate entry', (done) => {
		chai.request(domain)
		.post('/currency')
		.type('json')
		.send({
			'alias': 'usd',
				'name': 'United States Dollar',
				'ex': {
				  'peso': 50.73,
				  'won': 1187.24,
				  'yen': 108.63,
				  'yuan': 7.03
			}

		})
		.end((err, res) =>{
			expect(res.status).to.equal(400); //Bad request
			done()
		})
	})

	// 12. Check if post /currency returns status 200 if all fields are complete and there are no duplicates
	// is located in test.js since this test suite will conflict cause all above will send 400, thus failing this test
})
